package models;

public class ImageJSON {
	
	private String path;
	private int width;
	private int height;
	private double kb;
	
	public ImageJSON(String path, int width, int height, double kb){
		this.path = path;
		this.width = width;
		this.height = height;
		this.kb = kb;
	}

	public double getKb() {
		return kb;
	}

	public void setKb(double kb) {
		this.kb = kb;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
}
