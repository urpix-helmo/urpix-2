package models;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;

public class Image {
	
	BufferedImage image;
	BufferedImage originalImage;
	int[] originalSize = new int[2];
	
	
	public void rescale(int offset)
	{
			RescaleOp rescaleOp = new RescaleOp(1.2f, offset, null);
			rescaleOp.filter(originalImage, image);
	}
	
	public void grayscale()
	{
		//TODO: go backward after crop doesn't work.
		int p, r, g, b, avg;
		for(int y = 0; y < image.getHeight(); y++)
		{
			for(int x = 0; x < image.getWidth(); x++)
			{
				p = image.getRGB(x, y);
				if(p == originalImage.getRGB(x, y)){
					r = (p>>16) &0xff;
					g = (p>>8) &0xff;
					b = p&0xff;
					
					avg = (r+g+b)/3;
					
					p = (avg<<24) | (avg<<16) | (avg<<8) | avg;
					image.setRGB(x, y, p);
					
				} else image.setRGB(x, y, originalImage.getRGB(x, y));
				
			}
		}
	}
	
	public BufferedImage getImage()
	{
		return this.image;
	}
	
	public void setImage(BufferedImage image)
	{
		this.image = image;
	}
	
	public void setOriginalImage(BufferedImage image)
	{
		this.originalImage = image;
	}
	
	public BufferedImage getOriginalImage()
	{
		return this.originalImage;
	}

	public void setOriginalSize(int width, int height) {
		originalSize[0] = width;
		originalSize[1] = height;
		
	}

	public int getOriginalWidth() {
		return originalSize[0];
	}

	public int getOriginalHeight() {
		return originalSize[1];
	}
	
	public BufferedImage cropImage(Point entryClick, Point exitClick) {
		//TODO add conditions for Y coordinate
		int width = 0, height = 0;
		if(exitClick == null) return image;
		if(exitClick.getX() > entryClick.getX())
		{
			width = (int)exitClick.getX() - (int)entryClick.getX();
			height = (int)exitClick.getY() - (int)entryClick.getY();
		}
		else if(exitClick.getX() < entryClick.getX())
		{
			width = (int)entryClick.getX() - (int)exitClick.getX();
			height = (int)entryClick.getY() - (int)exitClick.getY();
		}
			BufferedImage dest = image.getSubimage((int)entryClick.getX(), (int)entryClick.getY(), width, height);
	      
			return dest;
	   }
	
}

