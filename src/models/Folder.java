package models;
import java.util.ArrayList;

public class Folder {
	private String name;
	private ArrayList<String> images;
	
	public Folder(String name) {
		this.name = name;
		images = new ArrayList<String>();
	}
	
	public String getName() {
		return name;
	}
	
	public ArrayList<String> getImages() {
		return images;
	};
	
	public void addImage(String path) {
		images.add(path);
	}
	
	public void clearImages() {
		images.clear();
	}
}
