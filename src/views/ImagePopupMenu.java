package views;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import controllers.MainWindowController;
import models.Folder;

public class ImagePopupMenu extends JPopupMenu {
	private static final long serialVersionUID = 1L;
	private JMenuItem editItem;
	private JMenuItem folderItem;
	private Object[] folders;
	private MainWindowController controller;
	private ArrayList<JLabel> selectedImages;
	
    public ImagePopupMenu(ArrayList<Folder> folders, MainWindowController controller, ArrayList<JLabel> selectedImages){
    	this.controller = controller;
    	this.selectedImages = selectedImages;
    	
        editItem = new JMenuItem("Edit image...");
        folderItem = new JMenuItem("Move to folder...");

        this.folders = new Object[folders.size()];
        for(int i = 0; i < folders.size(); i++) {
        	this.folders[i] = folders.get(i).getName();
        }
        
        addActionListeners();
        addComponents();    
    }

	private void addActionListeners() {
		editItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
			        Component invoker = getInvoker();					
					CorrectionWindow correctionWindow = new CorrectionWindow(invoker.getName());
					correctionWindow.launch();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		});
        
        folderItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedFolder = (String)JOptionPane.showInputDialog(null, "Choose destination folder: ", "Move image to folder", JOptionPane.PLAIN_MESSAGE, null, folders, folders[0]);
				for(JLabel image : selectedImages) {
					String sourcePath = image.getName();
					File imagePath = new File(sourcePath);
					
					String imageName = sourcePath.substring(sourcePath.lastIndexOf("\\"));
					String sourceFolder = sourcePath.substring(0, sourcePath.lastIndexOf("\\"));
					String destPath;
					
					if(selectedFolder != null) {
						if(selectedFolder.equals("All")) {
							destPath = sourceFolder.substring(0, sourceFolder.lastIndexOf("\\")+1) + imageName;
						} else {
							destPath = sourceFolder.substring(0, sourceFolder.lastIndexOf("\\")+1) + selectedFolder + imageName;
						}	
						imagePath.renameTo(new File(destPath));
						
						if(selectedImages.size() <= 1) {
							controller.setOutputMessage(imageName.substring(1) + " successfully moved to " + selectedFolder + ". Refresh by clicking another folder.");
						} else {
							controller.setOutputMessage("Images successfully moved to " + selectedFolder + ". Refresh by clicking another folder.");
						}
					} else {
						controller.setOutputMessage("");
					}
				}
			}
		});
	}

	private void addComponents() {
		if(selectedImages.size() <= 1)
			add(editItem);
        add(folderItem);		
	}
}