package views;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import controllers.CorrectImageController;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JLabel;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.*;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

public class CorrectionWindow {

	private JFrame frame;
	private CorrectImageController controller;
	private Point entryClick, exitClick;
	private String imageName;
	
	//TODO: delete test file if exit without submitted
	//TODO: change resize for traitment on another image
	
	/**
	 * Launch the application.
	 */
				
	public void launch() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				CorrectionWindow window;
				try {
					window = new CorrectionWindow(imageName);
					window.frame.setVisible(true);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}	
	
	/**
	 * Create the application.
	 * @param name 
	 * @throws IOException 
	 */
	public CorrectionWindow(String imageName) throws IOException {
		controller = new CorrectImageController();
		this.imageName = imageName;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() throws IOException {
		controller.setOriginalImage(ImageIO.read(new File(imageName)));
		controller.setImage(ImageIO.read(new File(imageName)));
		
		controller.setOriginalSize(controller.getImage().getWidth(), controller.getImage().getHeight());
		controller.setImage((BufferedImage)scaleImage(controller.getImage(), 350));
		controller.setOriginalImage((BufferedImage)scaleImage(controller.getImage(), 350));
		
		frame = new JFrame();
		frame.setBounds(100, 100, 784, 479);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
			
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					controller.setImage((BufferedImage)resizeImage(controller.getImage(), controller.getOriginalWidth(), controller.getOriginalHeight()));
					ImageIO.write(controller.getImage(), "jpg", new File("test.jpg"));
					
				} catch (IOException e) {
					System.out.println(e);
				}
			}
		});
		springLayout.putConstraint(SpringLayout.SOUTH, btnSubmit, -39, SpringLayout.SOUTH, frame.getContentPane());
		frame.getContentPane().add(btnSubmit);
		
		JLabel lblImage = new JLabel();
		springLayout.putConstraint(SpringLayout.SOUTH, lblImage, -24, SpringLayout.SOUTH, frame.getContentPane());
		lblImage.setVerticalAlignment(SwingConstants.TOP);
		springLayout.putConstraint(SpringLayout.WEST, btnSubmit, 104, SpringLayout.EAST, lblImage);
		springLayout.putConstraint(SpringLayout.EAST, lblImage, 414, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblImage, 27, SpringLayout.WEST, frame.getContentPane());
		lblImage.setHorizontalAlignment(SwingConstants.LEFT);
				
		ImageIcon image = new ImageIcon(controller.getImage());
		lblImage.setIcon(image);
		lblImage.addMouseListener(new MouseAdapter() {
			Graphics2D g2d;
			Rectangle rect;
			@Override
			public void mousePressed(MouseEvent e) {
				entryClick = new Point((int) MouseInfo.getPointerInfo().getLocation().getX()-(int)lblImage.getLocationOnScreen().getX(), 
						(int) MouseInfo.getPointerInfo().getLocation().getY()-(int)lblImage.getLocationOnScreen().getY());
				
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				exitClick = new Point((int) MouseInfo.getPointerInfo().getLocation().getX()-(int)lblImage.getLocationOnScreen().getX(), 
						(int) MouseInfo.getPointerInfo().getLocation().getY()-(int)lblImage.getLocationOnScreen().getY());
				
				if(g2d != null)
				{
					
					g2d.clearRect(0, 0, controller.getImage().getWidth(), controller.getImage().getWidth());
					g2d.dispose();
					
					lblImage.paint(g2d);
				} else {
				g2d = controller.getImage().createGraphics();
				g2d.setColor(Color.WHITE);
				rect = new Rectangle((int)entryClick.getX(), (int)entryClick.getY(), (int)(exitClick.getX() - entryClick.getX()), (int) (exitClick.getY() - entryClick.getY()));
				g2d.draw(rect);
				lblImage.repaint();
				}
			}
			
		});
		
		
		JSlider slider = new JSlider();
		springLayout.putConstraint(SpringLayout.EAST, slider, -63, SpringLayout.EAST, frame.getContentPane());
		slider.setMinimum(1);
		slider.setValue(50);
		slider.setMaximum(100);
		slider.addMouseMotionListener(new MouseMotionAdapter() { //TODO: change value for real saturation
			@Override
			public void mouseDragged(MouseEvent e) {
				
				int rgb, red, green, blue;
				float saturation = slider.getValue()/100f;
				for(int x = 0; x < controller.getImage().getWidth(); x++)
				{
					for(int y = 0; y < controller.getImage().getHeight(); y++)
					{
						
						if(saturation >= 0.4f && saturation <= 0.6f)
							controller.getImage().setRGB(x, y, controller.getOriginalImage().getRGB(x, y));
						
						rgb = controller.getImage().getRGB(x, y);
						red = (rgb >> 16) & 0xff;
						green = (rgb >> 8) & 0xff;
						blue = (rgb) & 0xff;

						float[] hsb = Color.RGBtoHSB(red, green, blue, null);

						float hue = hsb[0];
						float brightness = hsb[2];

						int rgb2 = Color.HSBtoRGB(hue, saturation, brightness);

						red = (rgb2>>16)&0xFF;
						green = (rgb2>>8)&0xFF;
						blue = rgb2&0xFF;
						
						controller.getImage().setRGB(x, y, rgb2);
						
					}
				}
				lblImage.setIcon(new ImageIcon(controller.getImage()));
				lblImage.repaint();
			}
		});
		frame.getContentPane().add(slider);
		
		JLabel lblLuminosity = new JLabel("Saturation :");
		springLayout.putConstraint(SpringLayout.WEST, lblLuminosity, 0, SpringLayout.WEST, slider);
		springLayout.putConstraint(SpringLayout.SOUTH, lblLuminosity, -6, SpringLayout.NORTH, slider);
		frame.getContentPane().add(lblLuminosity);
		
		JLabel lblContrast = new JLabel("Lightness :");
		springLayout.putConstraint(SpringLayout.WEST, slider, 0, SpringLayout.WEST, lblContrast);
		springLayout.putConstraint(SpringLayout.SOUTH, slider, -24, SpringLayout.NORTH, lblContrast);
		frame.getContentPane().add(lblContrast);
		
		
		JSlider slider_2 = new JSlider();
		
		springLayout.putConstraint(SpringLayout.WEST, slider_2, 44, SpringLayout.EAST, lblImage);
		springLayout.putConstraint(SpringLayout.EAST, slider_2, -63, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblContrast, 0, SpringLayout.WEST, slider_2);
		springLayout.putConstraint(SpringLayout.SOUTH, lblContrast, -6, SpringLayout.NORTH, slider_2);
		slider_2.setValue(0);
		slider_2.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				controller.rescale(slider_2.getValue());
				lblImage.repaint();
			}
			
		});		
		
		slider_2.setMaximum(200);
		slider_2.setMinimum(-200);
		frame.getContentPane().add(slider_2);
		frame.getContentPane().add(lblImage);
		
		JButton btnCrop = new JButton("Crop");
		springLayout.putConstraint(SpringLayout.NORTH, lblImage, 20, SpringLayout.SOUTH, btnCrop);
		springLayout.putConstraint(SpringLayout.WEST, btnCrop, 83, SpringLayout.WEST, frame.getContentPane());
		btnCrop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				controller.setImage(controller.cropImage(entryClick, exitClick));
				try {
					ImageIO.write(controller.getImage(), "jpg", new File("test.jpg"));
					controller.setOriginalImage(ImageIO.read(new File("test.jpg")));
				} catch (IOException e) {
					System.out.println(e);
				}
				
				lblImage.setIcon(new ImageIcon(controller.getImage()));
				entryClick = null; exitClick = null;
			}
		});
		frame.getContentPane().add(btnCrop);
		
		JButton btnGrayscale = new JButton("Grayscale");
		springLayout.putConstraint(SpringLayout.WEST, btnGrayscale, 203, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnGrayscale, -456, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, btnCrop, 0, SpringLayout.NORTH, btnGrayscale);
		springLayout.putConstraint(SpringLayout.EAST, btnCrop, -53, SpringLayout.WEST, btnGrayscale);
		springLayout.putConstraint(SpringLayout.NORTH, btnGrayscale, 22, SpringLayout.NORTH, frame.getContentPane());
		btnGrayscale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.grayscale();
				lblImage.repaint();
			}
		});
		frame.getContentPane().add(btnGrayscale);
		
		JLabel lblHue = new JLabel("Hue :");
		springLayout.putConstraint(SpringLayout.SOUTH, slider_2, -33, SpringLayout.NORTH, lblHue);
		frame.getContentPane().add(lblHue);
		
		JSlider slider_1 = new JSlider();
		slider_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				try {
					ImageIO.write(controller.getImage(), "jpg", new File("test.jpg"));
					controller.setOriginalImage(ImageIO.read(new File("test.jpg")));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		

		springLayout.putConstraint(SpringLayout.NORTH, slider_1, 255, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, slider_1, 44, SpringLayout.EAST, lblImage);
		springLayout.putConstraint(SpringLayout.SOUTH, slider_1, -71, SpringLayout.NORTH, btnSubmit);
		springLayout.putConstraint(SpringLayout.EAST, slider_1, -63, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblHue, 0, SpringLayout.WEST, slider_1);
		springLayout.putConstraint(SpringLayout.SOUTH, lblHue, -6, SpringLayout.NORTH, slider_1);
		slider_1.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				
				float hue = slider_1.getValue()/100.0f;
				int rgb, red, green, blue;
				float[] hsb;
				
				for(int x = 0; x < controller.getImage().getWidth(); x++)
					{
					for(int y = 0; y < controller.getImage().getHeight(); y++)
						{
						
							if(hue >= 0.49f && hue <= 0.51f)
								controller.getImage().setRGB(x, y, controller.getOriginalImage().getRGB(x, y));
							else
							{
								rgb = controller.getImage().getRGB(x, y);
								red = (rgb >> 16) & 0xff;
								green = (rgb >> 8) & 0xff;
								blue = (rgb) & 0xff;
								
								hsb = new float[3];
								Color.RGBtoHSB(red, green, blue, hsb);
								controller.getImage().setRGB(x, y, Color.getHSBColor(hue, hsb[1], hsb[2]).getRGB());
							}
							
						}
					}
				lblImage.setIcon(new ImageIcon(controller.getImage()));
				lblImage.repaint();
				
			}
		});
		frame.getContentPane().add(slider_1);
		
	}
	
	//------------------------

	
	/*private BufferedImage cropImage() {
		//TODO add conditions for Y coordinate
		int width = 0, height = 0;
		if(exitClick == null) return controller.getImage();
		if(exitClick.getX() > entryClick.getX())
		{
			width = (int)exitClick.getX() - (int)entryClick.getX();
			height = (int)exitClick.getY() - (int)entryClick.getY();
		}
		else if(exitClick.getX() < entryClick.getX())
		{
			width = (int)entryClick.getX() - (int)exitClick.getX();
			height = (int)entryClick.getY() - (int)exitClick.getY();
		}
			BufferedImage dest = controller.getImage()
	    		  .getSubimage((int)entryClick.getX(), (int)entryClick.getY(), width, height);
	      
			return dest;
	   }*/
	
	private Image resizeImage (Image source, int width, int height){
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	    Graphics2D g = (Graphics2D) img.getGraphics();
	    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g.drawImage(source, 0, 0, width, height, null);
	    g.dispose();
	    return img;
	}
	
	public Image scaleImage(Image source, int size) {
	    int width = source.getWidth(null);
	    int height = source.getHeight(null);
	    double f = 0;
	    if (width < height) {//portrait
	        f = (double) height / (double) width;
	        width = (int) (size / f);
	        height = size;
	    } else {//landscape
	        f = (double) width / (double) height;
	        width = size;
	        height = (int) (size / f);
	    }
	    return resizeImage(source, width, height);
	}

}
	       
