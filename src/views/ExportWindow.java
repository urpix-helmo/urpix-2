package views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controllers.ExportWindowController;
import models.Folder;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class ExportWindow extends JFrame{

	private ExportWindowController controller;
	private JSpinner spinWidth1;
	private JSpinner spinWidth2;
	private JSpinner spinHeight1;
	private JSpinner spinHeight2;
	private ArrayList<Folder> folders = new ArrayList<>(); 
	protected Object frmUrpix;
	private JTextField textField;
	
	public void launch() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				ExportWindow window = new ExportWindow(folders);
				setVisible(true);
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ExportWindow(ArrayList<Folder> folders) {
		initialize();
		this.folders = folders;
		this.controller = new ExportWindowController(this);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("Export pictures");
		setBounds(100, 100, 434, 307);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JButton btnExport = new JButton("Export");
		btnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!textField.getText().startsWith("C") && !textField.getText().startsWith("/")){
					JOptionPane.showMessageDialog(null, "Choose a path");
				} else {
					controller.exportImages((Integer)getSpinWidth1().getValue(), (Integer)getSpinWidth2().getValue(), textField.getText());
					close();
				}
			}
		});
		btnExport.setBounds(319, 234, 89, 23);
		panel.add(btnExport);
		
		JLabel lblChooseYourResolution = new JLabel("Pictures resolution");
		lblChooseYourResolution.setBounds(25, 61, 179, 14);
		panel.add(lblChooseYourResolution);
		
		JLabel lblVignettesResolution = new JLabel("Vignettes resolution");
		lblVignettesResolution.setBounds(25, 157, 179, 14);
		panel.add(lblVignettesResolution);
		
		JLabel lblWidth = new JLabel("width");
		lblWidth.setBounds(35, 92, 46, 14);
		panel.add(lblWidth);
		
		JLabel lblHeight = new JLabel("height");
		lblHeight.setBounds(35, 117, 46, 14);
		panel.add(lblHeight);
		
		JLabel label = new JLabel("width");
		label.setBounds(35, 182, 46, 14);
		panel.add(label);
		
		JLabel label_1 = new JLabel("height");
		label_1.setBounds(35, 207, 46, 14);
		panel.add(label_1);
		
		spinWidth1 = new JSpinner();
		spinWidth1.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinWidth1.setBounds(91, 86, 61, 20);
		panel.add(spinWidth1);
		
		spinHeight1 = new JSpinner();
		spinHeight1.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinHeight1.setBounds(91, 111, 61, 20);
		panel.add(spinHeight1);
		
		spinWidth2 = new JSpinner();
		spinWidth2.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinWidth2.setBounds(91, 179, 61, 20);
		panel.add(spinWidth2);
		
		spinHeight2 = new JSpinner();
		spinHeight2.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinHeight2.setBounds(91, 204, 61, 20);
		panel.add(spinHeight2);
		
		textField = new JTextField();
		textField.setBounds(25, 22, 210, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(2);
				int value = chooser.showOpenDialog(null);
				if (value == JFileChooser.APPROVE_OPTION && !chooser.getSelectedFile().getName().contains(".")){
					textField.setText(chooser.getSelectedFile().getPath());
				}
			}
		});
		btnBrowse.setBounds(244, 21, 89, 23);
		panel.add(btnBrowse);
			
	}
	
	public JSpinner getSpinWidth1() {
		return spinWidth1;
	}

	public void setSpinWidth1(JSpinner spinWidth1) {
		this.spinWidth1 = spinWidth1;
	}

	public JSpinner getSpinWidth2() {
		return spinWidth2;
	}

	public void setSpinWidth2(JSpinner spinWidth2) {
		this.spinWidth2 = spinWidth2;
	}

	public JSpinner getSpinHeight1() {
		return spinHeight1;
	}

	public void setSpinHeight1(JSpinner spinHeight1) {
		this.spinHeight1 = spinHeight1;
	}

	public JSpinner getSpinHeight2() {
		return spinHeight2;
	}

	public void setSpinHeight2(JSpinner spinHeight2) {
		this.spinHeight2 = spinHeight2;
	}

	public void close(){
		this.dispose();
	}
	
	public void setController(ExportWindowController controller){
		this.controller = controller;
	}
	
	public ArrayList<Folder> getFolders(){
		return this.folders;
	}
}
