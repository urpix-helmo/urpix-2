package views;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Color;

import javax.swing.SpringLayout;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import controllers.MainWindowController;
import models.Folder;

import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ScrollPaneConstants;
import javax.swing.JLabel;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = 1L;
	private MainWindowController controller;
	private ArrayList<Folder> folders;
	private JFrame frmUrpix;
	private String parentFolder;
	
	private DefaultListModel<String> lstFoldersModel;	
	private SpringLayout springLayout, sl_pnlLeft, sl_pnlBottom;
	private JPanel pnlLeft, pnlBottom, pnlRight;
	private JButton btnCreateFolder, btnExport, btnSelectFolder;
	private JList<String> lstFolders;
	private JScrollPane scrollPane;
	private JLabel lblOutput, lblNoImages;
	private GridBagConstraints lblNoImagesConstraints;

	public void launch() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				frmUrpix.setVisible(true);
			}
		});
	}
	
	public MainWindow() {	
		this.controller = new MainWindowController(this);
		folders = new ArrayList<Folder>();
		folders.add(new Folder("All"));
		
		initialize();
	}

	private void initialize() {	
		createComponents();
		setComponentLayout();
		addActionListeners();
		addComponents();
	}
	
	
	public void setOutputMessage(String output) {
		lblOutput.setText(output);
	}
	
	//GUI creation methods
	private void createComponents() {
		frmUrpix = new JFrame();		
		frmUrpix.setTitle("URPIX");
		frmUrpix.setBounds(100, 100, 1024, 768);
		frmUrpix.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		pnlLeft = new JPanel();
		pnlLeft.setBorder(new MatteBorder(0, 0, 0, 1, (Color) new Color(0, 0, 0)));
		
		pnlBottom = new JPanel();
		pnlBottom.setBorder(new MatteBorder(1, 0, 0, 0, (Color) new Color(0, 0, 0)));

		pnlRight = new JPanel(new GridBagLayout());		
		pnlRight.setBackground(new Color(211, 211, 211));
		
		btnCreateFolder = new JButton("Create new folder");
		btnCreateFolder.setToolTipText("Create a subfolder");
		btnCreateFolder.setFocusPainted(false);
		
		btnSelectFolder = new JButton("Select a folder");
		btnSelectFolder.setToolTipText("Select a folder");
		btnSelectFolder.setFocusPainted(false);
		
		btnExport = new JButton("Export");	
		btnExport.setToolTipText("Export images to JSON or HTML file");
		btnExport.setFocusPainted(false);

		scrollPane = new JScrollPane(pnlRight);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		lstFoldersModel = new DefaultListModel<>();
		lstFolders = new JList<String>(lstFoldersModel);

		lblOutput = new JLabel();
		lblOutput.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		lblNoImages = new JLabel("No images to show! Please select a folder.");
		lblNoImages.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNoImagesConstraints = new GridBagConstraints();
		lblNoImagesConstraints.fill = GridBagConstraints.HORIZONTAL;
		lblNoImagesConstraints.gridwidth = 3;
		lblNoImagesConstraints.gridx = 2;
		lblNoImagesConstraints.gridy = 3;
	}

	private void setComponentLayout() {
		springLayout = new SpringLayout();		
		springLayout.putConstraint(SpringLayout.NORTH, pnlLeft, 0, SpringLayout.NORTH, frmUrpix.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, pnlLeft, 0, SpringLayout.WEST, frmUrpix.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, pnlLeft, 0, SpringLayout.SOUTH, frmUrpix.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, pnlLeft, -806, SpringLayout.EAST, frmUrpix.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, pnlBottom, 0, SpringLayout.SOUTH, frmUrpix.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, pnlBottom, 0, SpringLayout.EAST, pnlLeft);
		springLayout.putConstraint(SpringLayout.EAST, pnlBottom, 0, SpringLayout.EAST, frmUrpix.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, pnlBottom, 0, SpringLayout.SOUTH, scrollPane);
		springLayout.putConstraint(SpringLayout.NORTH, scrollPane, 0, SpringLayout.NORTH, pnlLeft);
		springLayout.putConstraint(SpringLayout.WEST, scrollPane, 0, SpringLayout.EAST, pnlLeft);
		springLayout.putConstraint(SpringLayout.SOUTH, scrollPane, -43, SpringLayout.SOUTH, frmUrpix.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, scrollPane, 0, SpringLayout.EAST, pnlBottom);
		frmUrpix.getContentPane().setLayout(springLayout);

		sl_pnlLeft = new SpringLayout();
		sl_pnlLeft.putConstraint(SpringLayout.NORTH, btnCreateFolder, -35, SpringLayout.SOUTH, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.WEST, btnCreateFolder, 10, SpringLayout.WEST, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.SOUTH, btnCreateFolder, -10, SpringLayout.SOUTH, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.EAST, btnCreateFolder, -10, SpringLayout.EAST, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.NORTH, lstFolders, 41, SpringLayout.NORTH, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.WEST, lstFolders, 10, SpringLayout.WEST, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.SOUTH, lstFolders, -6, SpringLayout.NORTH, btnCreateFolder);
		sl_pnlLeft.putConstraint(SpringLayout.EAST, lstFolders, -10, SpringLayout.EAST, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.NORTH, btnSelectFolder, 10, SpringLayout.NORTH, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.WEST, btnSelectFolder, 10, SpringLayout.WEST, pnlLeft);
		sl_pnlLeft.putConstraint(SpringLayout.SOUTH, btnSelectFolder, -6, SpringLayout.NORTH, lstFolders);
		sl_pnlLeft.putConstraint(SpringLayout.EAST, btnSelectFolder, 0, SpringLayout.EAST, lstFolders);
		pnlLeft.setLayout(sl_pnlLeft);

		sl_pnlBottom = new SpringLayout();		
		sl_pnlBottom.putConstraint(SpringLayout.NORTH, btnExport, -32, SpringLayout.SOUTH, pnlBottom);
		sl_pnlBottom.putConstraint(SpringLayout.WEST, btnExport, 698, SpringLayout.WEST, pnlBottom);
		sl_pnlBottom.putConstraint(SpringLayout.SOUTH, btnExport, -7, SpringLayout.SOUTH, pnlBottom);
		sl_pnlBottom.putConstraint(SpringLayout.EAST, btnExport, -10, SpringLayout.EAST, pnlBottom);
		sl_pnlBottom.putConstraint(SpringLayout.NORTH, lblOutput, 0, SpringLayout.NORTH, btnExport);
		sl_pnlBottom.putConstraint(SpringLayout.WEST, lblOutput, -688, SpringLayout.WEST, btnExport);
		sl_pnlBottom.putConstraint(SpringLayout.SOUTH, lblOutput, 0, SpringLayout.SOUTH, btnExport);
		sl_pnlBottom.putConstraint(SpringLayout.EAST, lblOutput, -6, SpringLayout.WEST, btnExport);
		pnlBottom.setLayout(sl_pnlBottom);	
	}

	private void addActionListeners() {
		btnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ExportWindow expWindow = new ExportWindow(folders);
				expWindow.launch();
			}
		});
		
		lstFolders.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				setOutputMessage("Loading images...");
				if (!arg0.getValueIsAdjusting()) {	
					folders.clear();
					folders.add(new Folder("All"));
					
					controller.clearImagesFromFolders(folders);
					controller.addImagesToFolder(folders, parentFolder);
					controller.displayImages(pnlRight, folders, lstFolders.getSelectedValue());
					
					pnlRight.updateUI();
				}
				
			}
		});
		
		btnCreateFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(parentFolder != null) {
					String folderName = JOptionPane.showInputDialog("Enter folder name: ");
					if(folderName == null) {
						setOutputMessage("");
					} else if (folderName.length() <= 0) {
						setOutputMessage("Folder name can't be empty.");
					} else if (!folderName.matches("^[a-zA-Z0-9]*$")){
						setOutputMessage("Folder name can only have alphanumeric characters.");
					} else {
						File f = new File(parentFolder + "\\" + folderName);
						if (f.mkdir()) {
							setOutputMessage("Folder " + folderName + " created successfully.");
							folders.add(new Folder(f.getName()));
							controller.displayFolders(folders, lstFoldersModel, lstFolders);
						}
					}
				} else {
					setOutputMessage("Please select a folder before making subfolders.");
				}
			}
		});
		
		btnSelectFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				setOutputMessage("Loading images...");
				chooser.setFileSelectionMode(2);
				int value = chooser.showOpenDialog(null);
				if (value == JFileChooser.APPROVE_OPTION && !chooser.getSelectedFile().getName().contains(".")){				    
					folders.clear();
					folders.add(new Folder("All"));
					
					parentFolder = chooser.getSelectedFile().getPath();
					
					controller.addImagesToFolder(folders, parentFolder);
					controller.displayImages(pnlRight, folders, null);
					controller.displayFolders(folders, lstFoldersModel, lstFolders);
					pnlRight.updateUI();
				} else {
					setOutputMessage("No valid folder selected.");
				}
			}
		});
	}

	private void addComponents() {
		frmUrpix.getContentPane().add(pnlLeft);
		frmUrpix.getContentPane().add(pnlBottom);
		frmUrpix.getContentPane().add(scrollPane);
		pnlBottom.add(btnExport);
		pnlBottom.add(lblOutput);
		pnlLeft.add(lstFolders);
		pnlLeft.add(btnCreateFolder);
		pnlLeft.add(btnSelectFolder);
		pnlRight.add(lblNoImages, lblNoImagesConstraints);
	}
}
