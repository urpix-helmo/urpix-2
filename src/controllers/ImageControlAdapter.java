package controllers;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.border.Border;

import models.Folder;
import views.ImagePopupMenu;

public class ImageControlAdapter extends MouseAdapter {
	private ArrayList<Folder> folders;
	private MainWindowController controller;
	private ArrayList<JLabel> imageList, selectedImages;
	private Border selectedBorder, nonSelectedBorder;
	
	public ImageControlAdapter(ArrayList<Folder> folders, MainWindowController controller) { 
		this.folders = folders;
		this.controller = controller;
		selectedImages = new ArrayList<JLabel>();
		selectedBorder = BorderFactory.createLineBorder(Color.DARK_GRAY, 2);
		nonSelectedBorder = BorderFactory.createLineBorder(new Color(211, 211, 211), 2);
	}
	
	public void mousePressed(MouseEvent e){
		boolean refreshList = true;
		
        if (e.getButton() == MouseEvent.BUTTON3) {
        	if(selectedImages.contains((JLabel)e.getSource())) 
        		refreshList = false;
            showPopupMenu(e);
        } else if (e.isControlDown() && (e.getButton() == MouseEvent.BUTTON1)) {
        	refreshList = false;
        }
        
        if(refreshList) {
	    	selectedImages.clear();
	    	for(JLabel image : imageList) {
	    		image.setBorder(nonSelectedBorder);
	    	}
        }
        
        
        if(!selectedImages.contains((JLabel)e.getSource())) {
	        selectedImages.add((JLabel)e.getSource());
	        for(JLabel image : imageList) {
	        	for(JLabel selectedImage : selectedImages) {
	        		if(selectedImage == image) {
	        			image.setBorder(selectedBorder);
	        		}
	        	}
	        }
        }
    }

    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger())
            showPopupMenu(e);
    }

    private void showPopupMenu(MouseEvent e){
        ImagePopupMenu menu = new ImagePopupMenu(folders, controller, selectedImages);
        menu.show(e.getComponent(), e.getX(), e.getY());
    }
    
    public void setImageList(ArrayList<JLabel> imageList) {
    	this.imageList = imageList;
    }
}