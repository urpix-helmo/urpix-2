package controllers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import models.Folder;
import views.MainWindow;

public class MainWindowController{
	private ImageControlAdapter imageControlAdapter;
	private MainWindow mainWindow;
	
	public MainWindowController(MainWindow mainWindow){	
		this.mainWindow = mainWindow;
	}

	private Image resizeImage (Image source, int width, int height){
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g = (Graphics2D) img.getGraphics();
	    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g.drawImage(source, 0, 0, width, height, null);
	    g.dispose();
	    return img;
	}
	
	public Image scaleImage(Image source, int size) {
	    int width = source.getWidth(null);
	    int height = source.getHeight(null);
	    double f = 0;
	    if (width < height) {//portrait
	        f = (double) height / (double) width;
	        width = (int) (size / f);
	        height = size;
	    } else {//paysage
	        f = (double) width / (double) height;
	        width = size;
	        height = (int) (size / f);
	    }
	    return resizeImage(source, width, height);
	}

	public void addImagesToFolder(ArrayList<Folder> folders, String parentFolder) {	
		File f = new File(parentFolder);
		String[] fileList = f.list();	
		String folderName = parentFolder.substring(parentFolder.lastIndexOf("\\")+1);
		Folder currentFolder = searchFolderByName(folderName, folders);
		
		for (int i = 0; i < fileList.length; i++){
			if (fileIsImage(fileList[i])) {
				if (!currentFolder.getName().equals("All")) {
					folders.get(0).addImage(f.getPath() + '\\' + fileList[i]);
				}
				currentFolder.addImage(f.getPath() + '\\' + fileList[i]);
			} else if (!fileList[i].contains(".")){
				folderName = fileList[i].substring(fileList[i].lastIndexOf("\\")+1);
				folders.add(new Folder(folderName));
				addImagesToFolder(folders, parentFolder + "\\" + folderName);
			}
		}
	}
	
	public void displayImages(JPanel pnlDisplay, ArrayList<Folder> folders, String selectedFolder) {
		imageControlAdapter = new ImageControlAdapter(folders, this);
		pnlDisplay.removeAll();
		pnlDisplay.revalidate();
		pnlDisplay.repaint();
		Folder folder = folders.get(0);
		
		folder = searchFolderByName(selectedFolder, folders);
		
		GridBagConstraints c = new GridBagConstraints();
		int xCounter = 0;
		int yCounter = 0;
		
		if(!folder.getImages().isEmpty()) {
			for(String path : folder.getImages()) {
				try {
					BufferedImage picture = ImageIO.read(new File(path));
					Image zoom = scaleImage(picture, 150);
					
					JLabel lblImage = new JLabel(new ImageIcon(zoom));
					lblImage.addMouseListener(imageControlAdapter);
					lblImage.setToolTipText(path);
					lblImage.setName(path);
					lblImage.setBorder(BorderFactory.createLineBorder(new Color(211, 211, 211), 2));
					
					c.fill = GridBagConstraints.HORIZONTAL;
					c.gridx = xCounter;
					c.gridy = yCounter;
					c.insets = new Insets(1,1,1,1);
					
					pnlDisplay.add(lblImage, c);
					
					if (xCounter < 4) {
						xCounter++;
					} else {
						xCounter = 0;
						yCounter++;
					}
				} catch (IOException e) {
					e.printStackTrace();
					mainWindow.setOutputMessage("Failed to load images.");
				}
			}
			setOutputMessage("Images loaded successfully.");
		} else {
			JLabel lblNoImages = new JLabel("No images to show!");
			lblNoImages.setFont(new Font("Tahoma", Font.BOLD, 16));
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridwidth = 3;
			c.gridx = 2;
			c.gridy = 3;
			pnlDisplay.add(lblNoImages, c);
			
			setOutputMessage("This folder has no images.");
		}
		imageControlAdapter.setImageList(getImageList(pnlDisplay.getComponents()));
	}
	
	public void displayFolders(ArrayList<Folder> folders, DefaultListModel<String> lstFoldersModel, JList<String> lstFolders) {
		lstFoldersModel.removeAllElements();
		for(Folder f : folders) {
			lstFoldersModel.addElement(f.getName());
		}
		lstFolders.setModel(lstFoldersModel);
	}
	
	public boolean fileIsImage(String fileName) {
		String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		String[] validExtensions = {"jpg", "png", "gif", "jpeg"};

		if(Arrays.asList(validExtensions).contains(extension.toLowerCase())) {
			return true;
		} else {
			return false;
		}
	}
	
	public void clearImagesFromFolders(ArrayList<Folder> folders) {
		for(Folder f : folders) {
			f.clearImages();
		}
	}
	
	private Folder searchFolderByName(String folderName, ArrayList<Folder> folders) {
		if (folderName != null) {
			for (Folder f : folders) {
				if(folderName.equals(f.getName()))
					return f;
			}
		}
		return folders.get(0);
	}
	
	private ArrayList<JLabel> getImageList(Component[] images) {
		ArrayList<Component> components = new ArrayList<Component>(Arrays.asList(images));
		ArrayList<JLabel> imageLabels = new ArrayList<JLabel>();
		for (Component c : components) {
			imageLabels.add((JLabel)c);
		}
		return imageLabels;
	}
	
	public void setOutputMessage(String output) {
		mainWindow.setOutputMessage(output);
	}
}
