package controllers;

import java.awt.Graphics2D;

import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.Folder;
import models.ImageJSON;
import views.ExportWindow;

public class ExportWindowController{

	private ExportWindow window;
	
	public ExportWindowController(ExportWindow window) {
		super();
		this.setWindow(window);
	}

	public ExportWindow getWindow() {
		return window;
	}

	public void setWindow(ExportWindow window) {
		this.window = window;
		this.window.setController(this);
	}
	
	public void exportImages(int pictureWidth, int vignetteWidth, String path){
		
		ObjectMapper mapper = new ObjectMapper();
		File JsonFile = new File(path + "\\Description.json");
		ImageJSON img = null;
		ArrayList<ImageJSON> imgJSONs = new ArrayList<>();
		
		File mainFolder = new File(path + "\\Pictures");
		mainFolder.mkdir();
		for (int i = 0; i < this.getWindow().getFolders().size(); i++){
			File folder = new File(mainFolder + "\\" + this.getWindow().getFolders().get(i).getName());
			folder.mkdir();
			for (int j = 0; j < this.getWindow().getFolders().get(i).getImages().size(); j++){
				try {
					exportImg(i, j, "", pictureWidth, mainFolder, imgJSONs);
					exportImg(i, j, "vignette", vignetteWidth, mainFolder, imgJSONs);
					mapper.writeValue(JsonFile, imgJSONs);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void exportImg(int i, int j, String name, int width, File mainFolder, ArrayList<ImageJSON> imgJSONs) throws IOException{
		Icon icon = new ImageIcon(this.getWindow().getFolders().get(i).getImages().get(j));
		BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        icon.paintIcon(null, image.getGraphics(), 0, 0);
        BufferedImage picture = scaleImage(image, width);
        File f = new File(mainFolder + "\\" + this.getWindow().getFolders().get(i).getName() + "\\" + name + j + ".jpg");
		ImageIO.write(picture, "jpg", f);
		ImageJSON img = new ImageJSON(mainFolder + "\\" + this.getWindow().getFolders().get(i).getName() + "\\" + j + ".jpg",
							picture.getWidth(), picture.getHeight(), ((double)f.length()/1024));
		imgJSONs.add(img);
	}
	
	private BufferedImage resizeImage (BufferedImage source, int width, int height){
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	    Graphics2D g = (Graphics2D) img.getGraphics();
	    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g.drawImage(source, 0, 0, width, height, null);
	    g.dispose();
	    return img;
	}
	
	public BufferedImage scaleImage(BufferedImage source, int size) {
	    int width = source.getWidth(null);
	    int height = source.getHeight(null);
	    double f = 0;
	    if (width < height) {//portrait
	        f = (double) height / (double) width;
	        width = (int) (size / f);
	        height = size;
	    } else {//paysage
	        f = (double) width / (double) height;
	        width = size;
	        height = (int) (size / f);
	    }
	    return resizeImage(source, width, height);
	}
	
}
