package controllers;

import java.awt.Point;
import java.awt.image.BufferedImage;

import models.Image;

public class CorrectImageController {

	Image image = new Image();
	
	public void rescale(int offset)
	{
		image.rescale(offset);
	}
	
	public void grayscale()
	{
		image.grayscale();
	}
	
	public void setImage(BufferedImage bufferedImage)
	{
		image.setImage(bufferedImage);
	}
	
	public BufferedImage getImage()
	{
		return image.getImage();
	}
	
	public void setOriginalImage(BufferedImage bufferedImage)
	{
		image.setOriginalImage(bufferedImage);
	}
	
	public BufferedImage getOriginalImage()
	{
		return image.getOriginalImage();
	}

	public void setOriginalSize(int width, int height) {
		image.setOriginalSize(width, height);
		
	}

	public int getOriginalWidth() {
		return image.getOriginalWidth();
	}

	public int getOriginalHeight() {
		return image.getOriginalHeight();
	}
	
	public BufferedImage cropImage(Point entryClick, Point exitClick)
	{
		return image.cropImage(entryClick, exitClick);
	}
	

}
